#!/usr/bin/env bash

IFS=$'\n'
mounts=$(mount | grep cifs)
for i in $mounts
do
	mountpoint=$(echo "$i" | awk '{print($3)}')
	umount "$mountpoint"
done

