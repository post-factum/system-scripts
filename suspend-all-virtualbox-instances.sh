#!/usr/bin/env bash

users=`ps aux | grep VirtualBox | grep -v grep | cut -d ' ' -f 1 | sort | uniq`

for user in $users
do
	su $user -c "VBoxManage list runningvms | egrep -o -e '\{.*\}' | xargs -I {} VBoxManage controlvm {} savestate"
done
