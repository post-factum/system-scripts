#!/usr/bin/env python

import json, requests

countries_ch = ["switzerland.md", "france.md", "germany.md", "austria.md", "italy.md"]
countries_cz = ["czechia.md", "germany.md", "poland.md", "slovakia.md", "austria.md"]
countries_ua = ["ukraine.md", "poland.md", "belarus.md", "moldova.md", "romania.md", "hungary.md", "slovakia.md"]

servers = requests.get("https://publicpeers.neilalexander.dev/publicnodes.json")

dom = json.loads(servers.content)


print("Switzerland:")
hosts_ch = []
for country in dom:
    if country in countries_ch:
        for host in dom[country]:
            if '[' not in host and dom[country][host]["up"]:
                hosts_ch.append(host)
for host in hosts_ch:
    print("\"" + host + "\"", end = ", ")
print()

print("Czechia:")
hosts_cz = []
for country in dom:
    if country in countries_cz:
        for host in dom[country]:
            if '[' not in host and dom[country][host]["up"]:
                hosts_cz.append(host)
for host in hosts_cz:
    print("\"" + host + "\"", end = ", ")
print()

print("Ukraine:")
hosts_ua = []
for country in dom:
    if country in countries_ua:
        for host in dom[country]:
            if '[' not in host and dom[country][host]["up"]:
                hosts_ua.append(host)
for host in hosts_ua:
    print("\"" + host + "\"", end = ", ")
print()
 
