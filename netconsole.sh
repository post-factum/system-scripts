#!/usr/bin/env bash

NCONSOLE="/sys/kernel/config/netconsole"
TARGET="target1"
NCPATH="${NCONSOLE}/${TARGET}"

dmesg -n 8
modprobe netconsole

mkdir -p ${NCPATH}

echo 172.17.29.2 >${NCPATH}/local_ip
echo 172.17.29.1 >${NCPATH}/remote_ip
echo enp2s0 >${NCPATH}/dev_name
echo 40:16:7e:b2:6a:c5 >${NCPATH}/remote_mac

echo 1 >${NCPATH}/enabled

echo mark >/dev/kmsg

